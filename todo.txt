WeeWikiPaint

Minimum Marketing Feature
* marketing home page
- collaborative painting page
- wiki-fy pages
- more complex painting?
- more collaborative features?

User Stories:
* "hello world" server
- "hello world" client (one browser)
- support multiple browsers
- marketing copy

Engineering Tasks:
* Automated build
- static code analysis (lint)
- continuous integration
- server side testing
- client side testing


